﻿$('document').ready(function () {
    var start_date = document.getElementById("startDate");
    var end_date = document.getElementById("endDate");

    var today = new Date();
    var tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 1);
    tomorrow = tomorrow.toISOString().substr(0, 10);

    var max_date = new Date();
    max_date.setDate(today.getDate() + 1);
    max_date = max_date.toISOString().substr(0, 10);

    start_date.value = today.toISOString().substr(0, 10);
  // start_date.setAttribute("min", today).toISOString().substr(0,10);

    var end = new Date();
    end.setDate(end.getDate() + 30);
    end = end.toISOString().substr(0, 10);
    end_date.value = end;
    //end_date.setAttribute("max", end); 
    //end_date.setAttribute("min", tomorrow); 

    start_date.onchange = function () {

        var new_date = new Date(start_date.value);
        var next_day = new Date(start_date.value);

        next_day.setDate(next_day.getDate() + 1);
        next_day = next_day.toISOString().substr(0, 10);


        new_date.setDate = next_day.getDate() + 30;
        new_date = new_date.toISOString().substr(0, 10);

        end_date.value = next_date;
       // end_date.setAttribute("max", new_date);
       // end_date.setAttribute("min", new_date);
    }


    end_date.onchange = function () {
        calculatePrice();
    }

});


function calculatePrice() {

    var start_date = document.getElementById("startDate");
    var end_date = document.getElementById("endDate");

    var sec = Math.abs(new Date(end_date.value) - new Date(start_date.value)) / 1000;
    var days = Math.floor(sec / 86400);

    var price = parseFloat(document.getElementById("pricePerDay").textContent);
    var total_price = price * days;

    document.getElementById("totalPrice").value = total_price;

}
