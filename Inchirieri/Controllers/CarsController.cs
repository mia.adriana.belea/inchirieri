﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inchirieri.Models;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNet.Identity;

namespace Inchirieri.Controllers
{
    public class CarsController : Controller
    {

        public SqlConnection DbConnect()
        {
            string connectionString = @"Data Source=ADRIANA-LAPTOP\SQLEXPRESS; Initial Catalog=Inchirieri; Integrated Security = True;";
            SqlConnection conn = new SqlConnection(connectionString);
                 
            return conn;
        }

        public SqlDataReader ExecuteQuery(string cmd, SqlConnection conn)
        {
            SqlCommand sqlCmd = new SqlCommand(cmd, conn);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            return reader;

        }

        [Route ("cars")]
        public ActionResult Index()
        {
            List<Car> cars = new List<Car>();
            cars = CreateCars();
            ViewBag.Cars = cars;

            return View();
        }

        public Dictionary<int, string> GetTransmissions()
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlDataReader reader = ExecuteQuery("Select * From Transmissions", conn);
                       
            Dictionary<int, string> trs = new Dictionary<int, string>();
            while (reader.Read())
            {
                int trId = int.Parse(reader["tr_id"].ToString());
                string trVal = reader["tr_name"].ToString();

                trs[trId] = trVal;
            }

            return trs;

        }

        public Dictionary<int, string> GetFuelTypes()
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlDataReader reader = ExecuteQuery("Select * From FuelTypes", conn);
             
            Dictionary<int, string> fuel = new Dictionary<int, string>();
            while (reader.Read())
            {
                int fuelId = int.Parse(reader["fuel_id"].ToString());
                string fuelVal = reader["fuel_name"].ToString();

                fuel[fuelId] = fuelVal;
            }

            return fuel;

        }

        public Dictionary<int, string> GetColors()
        {
            {
                SqlConnection conn = DbConnect();
                conn.Open();
                SqlDataReader reader = ExecuteQuery("Select * From Colors", conn);

                Dictionary<int, string> color = new Dictionary<int, string>();
                while (reader.Read())
                {
                    int colorId = int.Parse(reader["color_id"].ToString());
                    string colorVal = reader["color_name"].ToString();

                    color[colorId] = colorVal;
                }

                return color;

            }

        }

        private List<Car> CreateCars()
        {
            List<Car> cars = new List<Car>();
            SqlConnection conn = DbConnect();
            conn.Open();

            String sqlCmd = "Select Cars.*, FuelTypes.fuel_name, Transmissions.tr_name, Colors.color_name From Cars, FuelTypes," +
                " Transmissions, Colors Where Cars.car_fuel=FuelTypes.fuel_id and Cars.car_transmission=Transmissions.tr_id AND Cars.car_color=Colors.color_id";

            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Car masina = new Car();
                masina.Id = int.Parse(reader["car_id"].ToString());
                masina.Model = reader["car_model"].ToString();
                masina.RegDate = int.Parse(reader["car_fabricatedAt"].ToString());
                masina.Fuel = reader["fuel_name"].ToString();
                masina.Seats= int.Parse(reader["car_seats"].ToString());
                masina.Color= reader["car_seats"].ToString();
                masina.Gearbox= reader["tr_name"].ToString();
                masina.Price= int.Parse(reader["car_rent_price"].ToString());

                cars.Add(masina);
            }
            conn.Close();
            return cars;
        }

        [HttpGet]
        [Route ("cars/edit/{CarId}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int CarId)
        {
            Car masina = new Car();
            List<Car> cars = CreateCars();
            if (CarId == -1)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == CarId);
            }
            Dictionary <int, string> fuel = GetFuelTypes();
            Dictionary <int, string> trs = GetTransmissions();

            ViewBag.FuelTypes = fuel;
            ViewBag.Transmissions = trs;

            return View(masina);

        }
         
        [HttpPost]
        [Route("cars/edit/{CarId}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int CarId, string carModel, int regDate, int fuelType, int numSeats, int carColor, int gearbox, int rentPrice)
        {
            string sqlCmd = "Update Cars Set car_model=@model, car_fabricatedAt=@regDate, car_fuel=@fuel, car_seats=@seats, car_color=@color, car_transmission=@trs, car_rent_price=@price, updatedAt=SYSDATETIME() where car_id=@id";
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);

            cmd.Parameters.AddWithValue("@model", carModel);
            cmd.Parameters.AddWithValue("@regDate", regDate);
            cmd.Parameters.AddWithValue("@fuel", fuelType);
            cmd.Parameters.AddWithValue("@seats",numSeats );
            cmd.Parameters.AddWithValue("@color",carColor );
            cmd.Parameters.AddWithValue("@trs", gearbox);
            cmd.Parameters.AddWithValue("@price", rentPrice);
            cmd.Parameters.AddWithValue("@id", CarId);

            cmd.ExecuteNonQuery();

            return RedirectToAction("Index");


        }

        [Route("cars/delete/{CarId}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete (int CarId)
        {
            SqlConnection conn = DbConnect();
            conn.Open();

            string sqlCmd = "Delete From Cars Where car_id= " + CarId;
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.ExecuteNonQuery();

            conn.Close();

            return RedirectToAction("/");

        }

        [HttpGet]
        [Route("cars/add")]
        [Authorize(Roles = "Admin")]
        public ActionResult Add()
        {
            Dictionary<int, string> fuel = GetFuelTypes();
            Dictionary<int, string> trs = GetTransmissions();
            Dictionary<int, string> colors = GetColors();

            ViewBag.FuelTypes = fuel;
            ViewBag.Transmissions = trs;
            ViewBag.Colors = colors;

            return View();
        }

        [HttpPost]
        [Route("cars/add")]
        public ActionResult Add(string carModel, int regDate, int fuelType, int numSeats, int carColor, int gearbox, int rentPrice)
        {
            string sqlCmd = "Insert into Cars (car_model, car_fabricatedAt, car_fuel, car_seats, car_color, car_transmission, car_rent_price, createdAt) " +
                "Values(@model, @regDate, @fuel, @seats, @color, @trs, @price, SYSDATETIME())";
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);

            cmd.Parameters.AddWithValue("@model", carModel);
            cmd.Parameters.AddWithValue("@regDate", regDate);
            cmd.Parameters.AddWithValue("@fuel", fuelType);
            cmd.Parameters.AddWithValue("@seats", numSeats);
            cmd.Parameters.AddWithValue("@color", carColor);
            cmd.Parameters.AddWithValue("@trs", gearbox);
            cmd.Parameters.AddWithValue("@price", rentPrice);
          

            cmd.ExecuteNonQuery();

            return RedirectToAction("Index");
                       
        }

        [HttpGet]
        [Route("cars/rent/{carId}")]
        public ActionResult Rent (int carId)
        {
            Car masina = new Car();
            List<Car> cars = CreateCars();
            if (carId == -1)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == carId);
            }
            return View(masina);
        }

        [HttpPost]
        [Route("cars/rent/{carId}")]
        [Authorize(Roles = "User")]
        public ActionResult Rent(int carId, string startDate, string endDate, int totalPrice)
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            var userId = User.Identity.GetUserId();

            string sqlCmd = "Insert into Rentals(order_carId, order_custId, order_startedAt, order_endedAt, order_totalPrice, createdAt) Values(@carId, @custId, @startDate, @endDate, @totalPrice, SYSDATETIME())";
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@carId", carId);
            cmd.Parameters.AddWithValue("@custId", userId);
            cmd.Parameters.AddWithValue("@startDate", startDate);
            cmd.Parameters.AddWithValue("@endDate", endDate);
            cmd.Parameters.AddWithValue("@totalPrice", totalPrice);

            cmd.ExecuteNonQuery();

            return RedirectToAction("../Orders");
        }


        [Route("cars/view/{carId}")]
        public ActionResult View(int CarId)
        {
            Car masina = new Car();
            List<Car> cars = CreateCars();

            if (CarId == -1)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == CarId);
            }

            ViewBag.image = "/Content/Images/car-" + CarId + ".jpg";
            return View(masina);
        }

        public ActionResult Random()
        {
            var car = new Car() { Id = 123, Model = "Berlina"};
            return View(car);
        }

        public ActionResult NotFound ()
        {
            return Content("404 Not Found");
            //return HttpNotFound();
        }

        public ActionResult Olx()
        {
            return Redirect("http://olx.ro");
        }

        public ActionResult Home ()
        {
            return RedirectToAction("Index", "Cars", new { page = 1 });
        }
               
    }
}